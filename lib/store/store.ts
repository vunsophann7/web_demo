
import {create} from "zustand"

export const useProductStore = create<{
    updateData: any,
    open: boolean,
    openSelectIitem: boolean,
    isUpdate: boolean,
    isPayerItem: boolean,
    setIsPayerItem: (isPayerItem: boolean) => void
    setOpenSelectIitem: (openSelectIitem: boolean) => void
    setOpen: (open: boolean) => void
    setUpdate: (isUpdate: boolean) => void
    setUpdateData: (updateData: any) => void
    setIsUpdate: (isUpdate: boolean) => void
    openProductDetail: boolean,
    setOpenProductDetail: (open: boolean) => void
    openCategory: boolean,
    setOpenCategory: (open: boolean) => void
}>(set => ({
    updateData: {},
    open: false,
    openSelectIitem: false,
    isUpdate: false,
    isPayerItem: false,
    setIsPayerItem: (isPayerItem: boolean) => set((state): any => ({...state, isPayerItem})),
    setOpenSelectIitem: (openSelectIitem: boolean) => set((state): any => ({...state, openSelectIitem})),
    setOpen: (open: boolean) => set((state): any => ({...state, open})),
    setUpdate: (isUpdate: boolean) => set((state): any => ({...state, isUpdate})),
    setIsUpdate: (isUpdate: boolean) => set((state): any => ({...state, isUpdate})),
    setUpdateData: (updateData: any) => set((state): any => ({...state, updateData})),
    openProductDetail: false,
    setOpenProductDetail: (openProductDetail: boolean) => set((state): any => ({...state, openProductDetail})),
    openCategory: false,
    setOpenCategory: (openCategory: boolean) => set((state): any => ({...state, openCategory}))
}))
