// "use client"
// import {useMutation, useQuery} from "@tanstack/react-query";
// import {productService} from "@/service/product.service";
// import {useState} from "react";
//
// const useFetchProducts = () => {
//     const [pageNumber, setPageNumber] = useState<number>(0);
//     const [pageSize, setPageSize] = useState<number>(10);
//     const query = useQuery({
//         queryKey: ["product", {pageNumber, pageSize}],
//         queryFn: async () => await productService.getProducts(pageNumber, pageSize)
//     })
//
//     return {
//         isLoading: query?.isLoading,
//         isError: query?.isError,
//         product: query?.data ?? []
//     }
// }
//
// export default useFetchProducts;