"use client"
import {useMutation, useQuery} from "@tanstack/react-query";
import {categoryService} from "@/service/category.service";

const useFetchCategory = () => {

    const query = useQuery({
        queryKey: ["category"],
        queryFn: async () => await categoryService.getCategory()
    })

    return {
        isLoading: query?.isLoading,
        isError: query?.isError,
        category: query?.data ?? []
    }
}

export default useFetchCategory;