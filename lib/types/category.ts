export interface Category {
    id: string;
    name: string;
    description: string
}

export interface CreateCategoryRequest {
    id: string;
    name: string;
    description: string;
}