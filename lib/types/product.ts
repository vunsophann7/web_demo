
export interface Product {
    id: string
    name: string;
    description: string;
    price: string;
    proDate: string;
    category: any
}

export interface Category {
    id: string;
    name: string;
    description: string;
}

export interface CreateProductRequest {
    name: string;
    description: string;
    price: string;
    category: string
}