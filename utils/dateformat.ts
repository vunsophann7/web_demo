import moment from "moment";

export const DateFormat = (value: any) => {
    if (!value) {
        return "";
    }
    let date = moment(value, 'YYYYMMDDHHmmss')
    return date.format("DD MMM YYYY")
}
export const StringToDate = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDD').format("DD MMM YYYY")
}

export const formatDate = (dateString:any) => {
    const date = moment(dateString, 'YYYYMMDD');
    if (!date.isValid()) {
        return 'Invalid date format';
    }
    return date.format("DD MMM YYYY");
}