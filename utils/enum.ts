export enum ProductType {
    STOCK = "STOCK",
    SOLD_OUT = "SOLD OUT",
    NEW = "NEW",
}