import {http} from "@/utils/http";
import {MessageFormat} from "@/utils/message-format";
import {User} from "@/lib/types";
import {CreateProductRequest, Product} from "@/lib/types/product";


const ServiceId = {
    PRODUCT: '/product',
}

const getProducts = async (pageNumber: number, pageSize: number)  => {
    try {
        const result = await http.get(ServiceId.PRODUCT, {
            params: {
                page_number: pageNumber,
                page_size: pageSize
            }
        });
        return result?.data;
    } catch (err) {
        return err;
    }
}

const createProduct = (data: CreateProductRequest) =>{
    return http.post(ServiceId.PRODUCT, data);
}

async function deleteProduct(id: any) {
    return http.delete(ServiceId.PRODUCT + '/' + id, {data: {product_ids: id }}).catch(error => error);
}

async function updateProduct(id: number,requestBody: any) {
    return http.put(ServiceId.PRODUCT + `/${id}`,requestBody).catch(error => error);
}

const getProductDetail = async (productId: any ): Promise<Product> => {
    return http.get(ServiceId.PRODUCT + `/${productId}`).then(res => res.data.data).catch(err => err);
}

export const productService = {
    createProduct,
    deleteProduct,
    updateProduct,
    getProducts,
    getProductDetail
}