"use client"
import {http} from "@/utils/http";
import {MessageFormat} from "@/utils/message-format";
import {Category, User} from "@/lib/types";
import {CreateCategoryRequest} from "@/lib/types/category";


const ServiceId = {
    CATEGORY: '/category',
}

const createCategory = (data: CreateCategoryRequest) => {
    return http.post(ServiceId.CATEGORY, data).catch(err => err);
}

const getCategory = async (): Promise<Category[]> => {
    const result = await http.get(ServiceId.CATEGORY)
    return result?.data
}

async function deleteCategory(id: any) {
    return http.delete(ServiceId.CATEGORY + '/' + id, {data: {category_ids: id }}).catch(error => error);
}

export const categoryService = {
    createCategory,
    getCategory,
    deleteCategory
}