import React from 'react';
import CategoryList from "@/components/ui/category/CategoryList";

const Page = () => {
    return (
        <div>
            <CategoryList />
        </div>
    );
};

export default Page;