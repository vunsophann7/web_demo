import React from 'react';
import ProductList from "@/components/ui/product/ProductList";

const Page = () => {
    return (
        <div>
            <ProductList />
        </div>
    );
};

export default Page;