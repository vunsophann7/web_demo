import React from 'react';
import UserList from "@/components/ui/dashboard/UserList";

const Page = () => {

    return (
        <div>
            <UserList />
        </div>
    );
};

export default Page;