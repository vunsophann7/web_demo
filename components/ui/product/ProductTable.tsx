import React, {FC, useState} from 'react';
import {
    Table,
} from "@tanstack/react-table";
import {flexRender} from "@tanstack/qwik-table";
import { Mutation, useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import {productService} from "@/service/product.service";
import {useProductStore} from "@/lib/store";
import ProductForm from "@/components/ui/product/ProductForm";
import {Button, Pagination} from "react-bootstrap";
import toast, { Toaster } from 'react-hot-toast';

type Props = {
    table: Table<any>
    handleRowClick?: (row: any) => void
}


const ProductTable: FC<Props> = ({ table, handleRowClick}) => {

    const queryClient = useQueryClient();
    const { open, setOpen, setIsUpdate, setUpdateData } = useProductStore(state => state);
    const [pageNumber, setPageNumber] = useState<number>(0);
    const [pageSize, setPageSize] = useState<number>(10);
    let lists:number[]  = [];
    var defFromLocal:number[] = [];


    const {data, isLoading, isError} = useQuery({
        queryKey: ["product", {pageNumber, pageSize}],
        queryFn: async () => await productService.getProducts(pageNumber, pageSize)
    })


    const handleDeleteProductMutate = useMutation(({
        mutationFn: (product_ids: number[]) => productService.deleteProduct(product_ids),
        onSuccess: async () => {
            toast.success("Product deleted successfully")
            queryClient.invalidateQueries({ queryKey: ['product'] })
        }
    }))

    const handleDeleteProduct = async (row: any) => {
        handleDeleteProductMutate.mutate(row);
    }



    return (
        <>
            <div className={"d-flex justify-content-between mt-3"}>
                <Button variant="primary" onClick={() => {
                    setOpen(true)
                    setUpdateData("")
                    setIsUpdate(false)
                }}>
                    Add
                </Button>
            </div>

            <table className="table table-sm table-hover table-bordered border-primary mt-3">
                <thead>
                {
                    table?.getHeaderGroups().map(headerGroup => (
                        <tr key={headerGroup.id} className={"table-active"}>
                            {headerGroup.headers.map((header, i) => (
                                <th
                                    className={(header.column.columnDef.meta as any)?.headerClass}
                                    key={header.id}
                                    colSpan={header.colSpan}
                                    style={{
                                        textAlign: 'center',
                                        display: lists?.some(a => a == i + 1) ? defFromLocal.some(a => a == i + 1) ? 'table-cell' : 'none' : 'table-cell'
                                    }}
                                >
                                    {header.isPlaceholder
                                        ? null
                                        : flexRender(
                                            header.column.columnDef.header,
                                            header.getContext()
                                        )}
                                </th>

                            ))}
                            <th className={"d-flex justify-content-center"}>Action</th>
                        </tr>
                    ))
                }
                </thead>
                <tbody>
                {
                    table.getRowModel().rows.length == 0
                        ?
                        <tr className="ks_brd_top ks_tbl_data_row" style={{textAlign: "center"}}>
                            <td className="ks_tbl_data"
                                colSpan={table.getVisibleFlatColumns().length}>Empty List
                                </td>
                            </tr>
                            :
                            table.getRowModel().rows.map(row => (
                                <tr className={"cursor-pointer ks_brd_top ks_tbl_data_row editable-row"}
                                    key={row.id}
                                    onClick={() => {
                                        if (handleRowClick) {
                                            handleRowClick(row.original)
                                        }
                                    }}>
                                    {row.getVisibleCells().map((cell, i) => (
                                        <td className={"ks_tbl_data"}
                                            align={(cell.column.columnDef.meta as any)?.align}
                                            style={{display: lists?.some(a => a == i + 1) ? defFromLocal.some(a => a == i + 1) ? 'table-cell' : 'none' : 'table-cell'}}
                                            key={cell.id}>
                                            {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                        </td>
                                    ))}
                                    <td className={"d-flex justify-content-center"}>
                                        <button className={"btn btn-danger me-2"}
                                                onClick={(e) => {
                                                    e.stopPropagation();// do not show tow popup
                                                    handleDeleteProduct(row.original.id).then(r => r);
                                                }}
                                        >Delete</button>
                                        <button
                                            className={"btn btn-info"}
                                            onClick={(e) => {
                                                e.stopPropagation();
                                                setOpen(true);
                                                setIsUpdate(true);
                                                setUpdateData(row.original);
                                            }}
                                        >
                                            Update
                                        </button>
                                    </td>
                                </tr>

                            ))
                }
                </tbody>
            </table>

            {
                open && <ProductForm />
            }
        </>
    );
};

// @ts-ignore
export default ProductTable;