import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import {useForm} from "react-hook-form";
import {CreateProductRequest} from "@/lib/types/product";
import {productService} from "@/service/product.service";
import useFetchCategory from "@/lib/hooks/use-fetch-category";
import { useQueryClient } from '@tanstack/react-query';
import {Button, Modal} from 'react-bootstrap';
import {useProductStore} from "@/lib/store";
import toast, { Toaster } from 'react-hot-toast';

const ProductForm = () => {
    const {category, isError, isLoading} = useFetchCategory();
    const queryClient = useQueryClient();
    const { open, setOpen, isUpdate, updateData } = useProductStore(state => state)

    const methods = useForm<
        Partial<
            CreateProductRequest
        >
    >({
        values: {
            name: updateData?.name || "",
            description: updateData?.description || "",
            price: updateData?.price || "",
            category: updateData?.category?.name || ""
        }
    });


    const {
        setValue,
        handleSubmit,
        register,
        watch,
        formState: {errors}
    } = methods;

    // handle create product mutation
    const onSubmit = async (data: any) => {
        const requestBody = {
            name: data?.name,
            description: data?.description,
            price: data?.price,
            category: data?.category
        };

        if (isUpdate) {
            const updateRes = await productService.updateProduct(updateData?.id, requestBody);
            if (updateRes?.status == 200) {
                setOpen(false)
                toast.success("Product updated successfully")
                queryClient.invalidateQueries({ queryKey: ['product'] })
            }
            return;
        }


        const resp = await productService.createProduct(requestBody);
        if (resp?.status == 200) {
            setOpen(false);
            toast.success("Product created successfully")
            queryClient.invalidateQueries({ queryKey: ['product'] })
        }

    }

    if (isError) {
        return <span>Error</span>;
    }

    if (isLoading) {
        return <span>Loading...</span>;
    }

    return (
        <>
            <Modal show={open}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="exampleModalLabel">New Product</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close" onClick={() => setOpen(false)}></button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="mb-3">
                                    <label htmlFor="recipient-name" className="col-form-label">Name:</label>
                                    <input type="text"
                                           className="form-control" placeholder={"Enter name"}
                                           id="recipient-name"
                                           {...register("name")}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="recipient-name" className="col-form-label">Description:</label>
                                    <input type="text" className="form-control" placeholder={"Enter description"}
                                           id="recipient-name"
                                           {...register("description")}
                                    />
                                    {
                                        <span>Name is required</span>
                                    }
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="recipient-name" className="col-form-label">Price:</label>
                                    <input type="text" className="form-control" placeholder={"Enter price"}
                                           id="recipient-name"
                                           {...register("price")}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="recipient-name" className="col-form-label">Category:</label>
                                    <select className="form-select"
                                            value={watch("category")}
                                            {...register("category")}
                                            aria-label="Default select example">
                                        <option value="" disabled>Choose a Category</option>
                                            {
                                                category?.data?.map((item: any) => {
                                                    return (
                                                        <option key={item.id} data-value={item.value}
                                                                value={item.id}>{item.name}</option>
                                                    )
                                                })
                                            }
                                    </select>
                                </div>

                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={() => setOpen(false)}>Close</button>
                            <button type="submit" className="btn btn-primary" onClick={handleSubmit(onSubmit)}>
                                {isUpdate ? "Update" : "Save"}
                            </button>
                        </div>
                    </div>
            </Modal>
</>

)
    ;
};

export default ProductForm;