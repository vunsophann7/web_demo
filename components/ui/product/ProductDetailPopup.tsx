"use client"
import React from 'react';
import {Modal} from "react-bootstrap";
import {useProductStore} from "@/lib/store";
import {Product} from "@/lib/types";
import {DateFormat} from "@/utils/dateformat";
import {ProductType} from "@/utils/enum";
interface Props {
    productDetailData: Product
}
const ProductDetailPopup = ({productDetailData: data}: Props) => {
    const { openProductDetail, setOpenProductDetail} = useProductStore(state => state)
    return (
        <>
            <Modal show={openProductDetail}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Product Detail</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal"
                                aria-label="Close" onClick={() => setOpenProductDetail(false)}></button>
                    </div>
                    <div className="modal-body">
                        <ul>
                            <li>{data?.name}</li>
                            <li>{data?.description}</li>
                            <li>{data?.price}</li>
                            <li>{data?.category?.name}</li>
                            <li>{DateFormat(data?.proDate)}</li>
                            <li>{ProductType.SOLD_OUT}</li>
                        </ul>
                    </div>
                    <div className="modal-footer">

                    </div>
                </div>
            </Modal>
        </>
    );
};

export default ProductDetailPopup;