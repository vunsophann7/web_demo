"use client";
import React, {FC, useState} from "react";
import ProductForm from "@/components/ui/product/ProductForm";
import ProductTable from "@/components/ui/product/ProductTable";
import { ColumnDef } from "@tanstack/table-core";
import { Product } from "@/lib/types/product";
import {getCoreRowModel, Table, useReactTable} from "@tanstack/react-table";
import {useQuery} from "@tanstack/react-query";
import {productService} from "@/service/product.service";
import PaginationProductComponent from "@/components/shared/PaginationProductComponent";
import ProductDetailPopup from "@/components/ui/product/ProductDetailPopup";
import {useProductStore} from "@/lib/store";
import Link from "next/link";
import toast, { Toaster } from 'react-hot-toast';
import {DateFormat, formatDate} from "@/utils/dateformat";
import {useSearchParams, usePathname, useRouter} from "next/navigation";

const defaultColumns: ColumnDef<Product>[] = [
  {
    accessorKey: "name",
    id: "name",
    header: "Name",
    cell: (props) => props.getValue(),
  },
  {
    accessorKey: "description",
    id: "description",
    header: "Description",
    cell: (props) => props.getValue(),
  },
  {
    accessorKey: "price",
    id: "price",
    header: "Price",
    cell: (props) => props.getValue(),
  },
  {
    accessorKey: "proDate",
    id: "proDate",
    header: "Create Date",
    cell: (props) => DateFormat(props.getValue()),
  },
  {
    accessorKey: "category",
    id: "category",
    header: "Category",
    cell: ({row}) => row.original?.category?.name,
  },
];
interface Props  {
  handleRowClick?: (row: any) => void,
  productDetailData: Product
}
const ProductList: FC<Props> = ({ handleRowClick}) => {
  const [pageNumber, setPageNumber] = useState<number>(0);
  const [pageSize, setPageSize] = useState<number>(10);
  const [productDetailData, setProductDetailData] = useState<Product>();
  const { openProductDetail, setOpenProductDetail } = useProductStore(state => state);
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const {replace} = useRouter();
  const {data, isLoading, isError} = useQuery({
    queryKey: ["product", {pageNumber, pageSize}],
    queryFn: async () => await productService.getProducts(pageNumber, pageSize)
  })
  const columns: ColumnDef<Product>[] = defaultColumns;
  const table = useReactTable({
    data: data?.data?.products,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  const handleRowProductClick = async (data: any) => {
    await productService.getProductDetail(data?.id).then(data => {
      setProductDetailData(data);
      setOpenProductDetail(true);
    }).catch(error => {
      console.error('Error fetching product detail:', error);
    });
  }

  if (isLoading) {
    return <span>Loading...</span>;
  }

  if (data?.isError) {
    return <span>Error</span>;
  }

  const handleSearchProduct = ( term: any) => {
    const params = new URLSearchParams(searchParams);
    if (term) {
      params.set("query", term);
    } else {
      params.delete('query');
    }
    replace(`${pathname}?${params.toString()}`);
  }


  return (
    <>

      <div className={"w-100 d-flex justify-content-center mt-3"}>
        <div className={"w-75"}>
          <nav className="navbar navbar-expand-sm bg-body-tertiary">
            <div className="container-fluid">
              <a className="navbar-brand" href="#">Demo</a>
              <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                      data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                      aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item">
                    <a className="nav-link" aria-current="page" href="">
                      <Link href={"/product"} className={"text-decoration-none"}>Product</Link>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="">
                      <Link href={"/category"} className={"text-decoration-none"}>Category</Link>
                    </a>
                  </li>
                </ul>
                <form className="d-flex" role="search">
                  <input className="form-control me-2" type="search"
                         onChange={(e) => handleSearchProduct(e.target.value)}
                         placeholder="Search" aria-label="Search"/>
                </form>
              </div>
            </div>
          </nav>
          <ProductForm/>
          <ProductTable table={table} handleRowClick={handleRowProductClick}/>
          <PaginationProductComponent data={data?.data?.pagination} page={(number, size) => {
            setPageNumber(number!);
            setPageSize(size!);
          }}
          />
        </div>
      </div>
      {
          openProductDetail && <ProductDetailPopup productDetailData={productDetailData!}/>
      }
        <Toaster />
    </>
  );
};

// @ts-ignore
export default ProductList;