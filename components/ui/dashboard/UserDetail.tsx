'use client'
import React from 'react';
import {useQuery} from "@tanstack/react-query";
import {userService} from "@/service/user.service";

type Props = {
    id: string;
}

const UserDetail = ({id}: Props) => {

    const {data, isLoading} = useQuery({
        queryKey: ["user", id],
        queryFn: () => userService.getUserById(id)
    });

    if(isLoading) {
        return <div>Loading...</div>
    }

    return (
        <article>
            <span>{data?.name}</span>
            <span>{data?.email}</span>
        </article>
    );
};

export default UserDetail;