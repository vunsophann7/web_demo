"use client"
import React from 'react';
import Link from "next/link";
import {useProductStore} from "@/lib/store";
import {Button} from "react-bootstrap";
import CategoryForm from "@/components/ui/category/CategoryForm";
import useFetchCategory from "@/lib/hooks/use-fetch-category";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import {productService} from "@/service/product.service";
import {categoryService} from "@/service/category.service";
import toast, { Toaster } from 'react-hot-toast';

const CategoryList = () => {
    const queryClient = useQueryClient();
    const { openCategory, setOpenCategory, isUpdate, updateData } = useProductStore(state => state)

    const {category} = useFetchCategory();
    queryClient.invalidateQueries({ queryKey: ['category'] })


    const handleDeleteCategoryMutate = useMutation(({
        mutationFn: (category_ids: number[]) => categoryService.deleteCategory(category_ids),
        onSuccess: async () => {
            toast.success("Category deleted successfully")
            queryClient.invalidateQueries({ queryKey: ['category'] })
        }
    }))

    const handleDeleteCategory = async (row: any) => {
        handleDeleteCategoryMutate.mutate(row)
    }

    return (
        <>
            <div className={"d-flex justify-content-center mt-5 navbar"}>
                <div className={"w-75"}>
                    <div className={"d-flex justify-content-between"}>
                        <button className={"btn btn-secondary"}>
                            <Link href={"/product"} className={"text-white link-underline-secondary"}>Back</Link>
                        </button>
                        <Button variant="primary" onClick={() => {
                            setOpenCategory(true)
                        }}>
                            Add
                        </Button>
                    </div>


                    <div className="container">
                        <div className="row row-cols-4 g-1 mt-3">
                            {
                                category?.data?.map((item) => {
                                    return (
                                        <>
                                            <div className={"col"}>
                                                <div key={item.id} className="card text-bg-info">
                                                    <div className="card-body">
                                                        <button type="button" className="btn btn-close"
                                                                onClick={() => handleDeleteCategory(item.id).then(err => err)}></button>
                                                        <h5 className="card-title">{item?.name}</h5>
                                                        <p className="card-text">{item?.description}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <Toaster />
                </div>
            </div>
            {
                openCategory && <CategoryForm/>
            }
        </>

    );
};

export default CategoryList;