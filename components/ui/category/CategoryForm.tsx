import React from 'react';
import {Modal} from "react-bootstrap";
import {useProductStore} from "@/lib/store";
import {useForm} from "react-hook-form";
import {CreateProductRequest} from "@/lib/types";
import {CreateCategoryRequest} from "@/lib/types/category";
import {categoryService} from "@/service/category.service";
import {useQueryClient} from "@tanstack/react-query";
import toast, { Toaster } from 'react-hot-toast';

const CategoryForm = () => {
    const queryClient = useQueryClient();
    const { openCategory, setOpenCategory, isUpdate, updateData } = useProductStore(state => state)

    const methods = useForm<
        Partial<
            CreateCategoryRequest
        >
    >({
        values: {
            name: "",
            description: "",
        }
    });

    const {
        setValue,
        handleSubmit,
        register,
        watch,
        formState: {errors}
    } = methods;
    const onSubmit = async (data) => {
        const requestBody = {
            name: data?.name,
            description: data?.description,
        }
        const resp = await categoryService.createCategory(requestBody)
        if (resp?.status == 200) {
            setOpenCategory(false)
            toast.success("Category created successfully")
            queryClient.invalidateQueries({ queryKey: ['category'] })
        }

    }
    return (
        <>
            <Modal show={openCategory}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">New Category</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal"
                                aria-label="Close" onClick={() => setOpenCategory(false)}></button>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="mb-3">
                                <label htmlFor="recipient-name" className="col-form-label">Name:</label>
                                <input type="text"
                                       className="form-control" placeholder={"Enter name"}
                                       id="recipient-name"
                                       {...register("name")}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="recipient-name" className="col-form-label">Description:</label>
                                <input type="text"
                                       className="form-control" placeholder={"Enter category"}
                                       id="recipient-name"
                                       {...register("description")}
                                />
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={() => setOpenCategory(false)}>Close</button>
                        <button type="submit" className="btn btn-primary" onClick={handleSubmit(onSubmit)}>
                            Save
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default CategoryForm;