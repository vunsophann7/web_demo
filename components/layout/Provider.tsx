"use client"
import React, {PropsWithChildren, useState} from 'react';
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
// 1. import `ChakraProvider` component
import { ChakraProvider } from '@chakra-ui/react'
const Provider = ({children}: PropsWithChildren) => {

    const [queryClient] = useState(new QueryClient({
        defaultOptions: {
            queries: {
                refetchOnWindowFocus: false
            }
        }
    }));

    return (
        <QueryClientProvider client={queryClient}>
            {children}
        </QueryClientProvider>
    );
};

export default Provider;