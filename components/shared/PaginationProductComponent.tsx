import React from 'react';
import {Pagination} from "@/lib/types/productPagination";
import cn from 'clsx'
const PaginationProductComponent = ({data: {total_pages, current_page, size, first, last, total_elements}, page}: { data: Pagination, page: (pageNumber?: number, pageSize?: number) => void }) => {

    console.log("current page : ", current_page)
    return (
        <div>
            <div className="ks_d_flex ks_alg_itm_ctr d-flex">
                <span className="ks_lbl">Page row</span>
                <div className="ks_ml20">
                    <select defaultValue={size} className="ks_select" title="ks_page_row" onChange={(e) => {
                        e.preventDefault()
                        page(0, Number(e.target.value))
                    }}>
                        {[10, 25, 50, 100].map((item, index) => (
                            <option key={index} value={item}>{item}</option>
                        ))}
                    </select>
                </div>

                <div className="ks_ml25">
                    <div className="ks_lbl ks_mg0">
                        {last ? total_elements : (current_page + 1) * size} {" of "}{" "}
                        {total_elements}
                    </div>
                </div>
            </div>
            <div className={"w-20 d-flex justify-content-between"}>

                <span title="first">
                        <a onClick={() => {
                            page(0, size)
                        }} className={cn("ks_wth24 ks_hgt24 ks_jump_prev_svg", {['disabled-page']: first})}>
                            {"|<<"}
                        </a>
                    </span>
                <span title="previous">
                        <a onClick={async () => {
                            page(current_page - 1, size)
                        }}
                           className={cn("ks_wth24 ks_hgt24 ks_prev_svg ks_ml20 disabled", {['disabled-page']: first})}>
                            {"<"}
                        </a>
                    </span>
                <span title="next">
                        <a onClick={async () => {
                            page(current_page + 1, size)
                        }}
                           className={cn("ks_wth24 ks_hgt24 ks_nx_svg ks_ml20", {['disabled-page']: last})}>
                            {">"}
                        </a>
                    </span>
                <span title="last">
                        <a onClick={() => {
                            page(total_pages - 1, size)
                        }} className={cn("ks_wth24 ks_hgt24 ks_jump_nx_svg ks_ml20", {['disabled-page']: last})}>
                            {">>|"}
                        </a>
                    </span>
            </div>
        </div>
    );
};

export default PaginationProductComponent;