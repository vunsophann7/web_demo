import {Pagination} from "@/lib/types/productPagination";
import { useRouter } from "next/router";
import cn from 'clsx'
const Pagination = ({data: { total_pages, current_page, size, first, last, total_elements }}: { data: Pagination}) => {
    const router = useRouter();

    return (
        <div className="ks_d_flex ks_flex_row ks_alg_itm_ctr ks_ptb20">
            <div className="ks_d_flex ks_alg_itm_ctr ks_comp_blk6">
                <span className="ks_lbl ks_mg0">Page row</span>
                <div className="ks_ml20">
                    <select defaultValue={size} className="ks_select" title="ks_page_row" onChange={(e) => {
                        e.preventDefault()
                        router.push({
                            pathname: router.pathname,
                            query: {
                                ...router.query,
                                page_size: e.target.value,
                                page_number: 0,
                            }
                        })
                    }}>
                        {[10, 25, 50, 100].map((item, index) => (
                            <option key={index}>{item}</option>
                        ))}
                    </select>
                </div>

                <div className="ks_ml25">
                    <div className="ks_lbl ks_mg0">
                        {last ? total_elements : (current_page + 1) * size} {"of"}{" "}
                        {total_elements}
                    </div>
                </div>
            </div>
            <div className="ks_d_flex ks_alg_itm_ctr">
                <div className="ks_ml30">
                    <span title="first">
                        <a onClick={() => {
                            router.push({
                                pathname: router.pathname,
                                query: {
                                    ...router.query,
                                    page_number: 0,
                                }
                            })
                        }} className={cn("ks_wth24 ks_hgt24 ks_jump_prev_svg", {['disabled-page']: first})}>
                            first
                        </a>
                    </span>
                    <span title="previous">
                        <a onClick={async () => {
                            await router.push({
                                pathname: router.pathname,
                                query: {
                                    ...router.query,
                                    page_number: current_page - 1,
                                }
                            })
                        }} className={cn("ks_wth24 ks_hgt24 ks_prev_svg ks_ml20", {['disabled-page']: first})}>
                            pre
                        </a>
                    </span>
                    <span title="next">
                        <a onClick={async () => {
                            await router.push({
                                pathname: router.pathname,
                                query: {
                                    ...router.query,
                                    page_number: current_page + 1
                                }
                            })
                        }} className={cn("ks_wth24 ks_hgt24 ks_nx_svg ks_ml20", {['disabled-page']: last})}>
                            next
                        </a>
                    </span>
                    <span title="last">
                        <a onClick={() => {
                            router.push({
                                pathname: router.pathname,
                                query: {
                                    ...router.query,
                                    page_number: total_pages - 1
                                }
                            })
                        }} className={cn("ks_wth24 ks_hgt24 ks_jump_nx_svg ks_ml20", {['disabled-page']: last})}>
                            last
                        </a>
                    </span>
                </div>
            </div>
        </div>
    );
};

export default Pagination;
